#pragma once
#include <memory>
#include <map>

#include "IShader.h"

class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

	void AddShader(std::string& p_id,std::shared_ptr<IShader>& p_shader); //so let's just take an IShader i guess?

	IShader* GetShader(std::string& p_id);


private:
	std::map<std::string, std::shared_ptr<IShader>> shaders;
};

