#include "TestState.h"

#include <iostream>
#include "Layer.h"
#include "Vector2.h"

/** Apparantly order of construction is not determined here, but in header, dafuq.*/
TestState::TestState() :
	time{0},
	testShader{},
	programID{ testShader.GetShaderID() },
	textureShader(),
	texturedProgramID{ textureShader.GetShaderID() },
	texture{ "../Assets/4kTest.jpg" },
	va{}, //since we are now dynamically drawing things, we don't really need this, but we'll keep it around since it may come in handy in the future.
	vb{ MAX_VERTEX_COUNT },
	ib{ MAX_INDEX_COUNT }
{
	textureShader.Bind();
	textureShader.SetUniform("vGridSize", 2000.0f);
	textureShader.SetUniform("vAtlasWidth", 4000.0f);
	textureShader.SetUniform("vAtlasHeight", 4000.0f);
	textureShader.SetUniform("vAtlasPosition", 0.0f);

	mat4 ide = identity();
	textureShader.SetUniformMat4("viewProjection", ide);
}

std::vector<GLuint> TestState::CreateIndices(float p_count) {
	
	std::vector<GLuint> indices;
	for (int i = 0; i < p_count; i++) {
		const GLuint step = 4*i;
		indices.push_back(0 + step);
		indices.push_back(1 + step);
		indices.push_back(2 + step);
		indices.push_back(1 + step);
		indices.push_back(3 + step);
		indices.push_back(2 + step);
	}
	return indices;
}

/**TODO: VEC2 DEPRECATED, REMOVE VEC2 AND REPLACE WITH VECTOR2!!!*/
std::array<Vertex,4> TestState::CreateQuad(float p_atlasPosition,float p_scale, Vec2 p_position)  {

	//std::array<Vertex,4> quads;

	Vertex v0;
	v0.positions = { -1.f * p_scale + p_position.x, -1.f * p_scale + p_position.y, 0.0f }; //bottom left
	v0.colors = { 0.0f, 0.0f, 1.0f };
	v0.uvs = { 0.0f, 1.0f };
	v0.atlasPosition = p_atlasPosition;

	Vertex v1;
	v1.positions = { -1.0f * p_scale + p_position.x ,1.0f * p_scale + p_position.y,0.0f }; //top left
	v1.colors = { 0.0f, 1.0f, 0.0f };
	v1.uvs = { 0.0f, 0.0f };
	v1.atlasPosition = p_atlasPosition;

	Vertex v2;
	v2.positions = { 1.0f * p_scale + p_position.x, -1.f * p_scale + p_position.y, 0.0f }; //bottom right
	v2.colors = { 0.0f, 1.0f, 0.0f };
	v2.uvs = { 1.0f,1.0f };
	v2.atlasPosition = p_atlasPosition;

	Vertex v3;
	v3.positions = { 1.0f * p_scale + p_position.x,1.0f * p_scale + p_position.y,0.0f }; //top right
	v3.colors = { 1.0f, 0.0f, 0.0f };
	v3.uvs = { 1.0f,0.0f };
	v3.atlasPosition = p_atlasPosition;

	return { v0,v1,v2,v3 };
}


TestState::~TestState()
{
}

void TestState::Enter()
{
	/*
	Vector2 testVec(11.f, 7.f);
	testVec.PrintVector("testVec");
	std::cout << "length = " << testVec.Length() << "\n";
	Vector2 normalized = testVec.Normalize();
	std::cout << "normalized = " << normalized.x << ", " << normalized.y << "\n";
	Vector2 testVec2(11.f, 12.f);
	testVec2.PrintVector("testVec2");
	std::cout << "dot product = " << testVec.Dot(testVec, testVec2) << " \n";
	testVec.DistanceTo(testVec, testVec2);
	testVec += (testVec2);
	testVec.PrintVector("testVec after increment operator");
	*/
}

void TestState::Exit()
{
}

void TestState::Update(float p_deltaTime)
{
	//std::cout << p_deltaTime << '\n';			blev s� fkn less p� delta time xD
	time += p_deltaTime;
	for (int i = 0; i < 1; i++) {
		DrawQuads();
	}
	//DrawQuads();

}


/** Consider moving some data to heap LOL. */
void TestState::DrawQuads() {
	//todo: allow ourselves to bind more than one texture slot at a time.
	//means we'll need to change our textureatlas gridsize & atlasWidth & height based on what texture we are using.

	textureShader.Bind(); //probably these should be moved to heap, this is testState tho so we don't really care.
	texture.Bind();

	std::array<Vertex, 4 * MAX_QUAD_COUNT> vertexData;

	for (int i = 0; i < MAX_QUAD_COUNT; i++) { //move to heap!
		Vec2 bajs = { -0.0f + /*i*0.2f +*/ sinf(time * 0.001f) * 0.01f,0.0f};
		auto quad = CreateQuad((float)(i % 4), 1.0f, bajs);
		memcpy(vertexData.data() + quad.size() * i, quad.data(), quad.size() * sizeof(Vertex));
	}
	std::vector<GLuint> indices = CreateIndices(MAX_QUAD_COUNT);

	va.Bind(); //We don't really need this when we are dynamically drawing, but it may still come in handy. costs basically nothing in terms of performance.
	vb.Bind(vertexData.size(), vertexData.data()); //size_t > unsigned int.
	ib.Bind(indices.size(), indices.data()		);

	textureShader.UpdateVertices();

	glDrawElements(GL_TRIANGLES, 6*MAX_QUAD_COUNT, GL_UNSIGNED_INT, 0);
}


void TestState::Draw() {

}