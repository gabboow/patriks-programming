#pragma once

#include "IState.h"
#include <string>
#include <map>

class StateMachine
{
public:
	StateMachine();
	~StateMachine();

	void AddState(std::string p_key,IState* p_stateToAdd);
	void SetState(std::string p_key);
	void Update(float p_deltaTime);
	void Draw();

private:
	std::map<std::string, IState*> states;
	IState* currentState;
};
