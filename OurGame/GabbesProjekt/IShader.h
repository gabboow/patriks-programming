#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <vector>

#include "Vertex.h"

class IShader
{
public:
	IShader();
	virtual ~IShader();

	virtual GLuint& GetShaderID() = 0;

	virtual void UpdateVertices() = 0;
	virtual void Bind() = 0;

	virtual void SetUniform(const std::string& p_uniform, float p_value) = 0;
	virtual void SetUniform(const std::string& p_uniform, int p_value) = 0;


private:


};

