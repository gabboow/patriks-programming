#pragma once

#include <memory>
#include "Layer.h"
#include "Renderer.h"
#include "LayerTypes.h"

//TODO: DEPRECATED CLASS(?)

class LayerRenderer {
public:
	LayerRenderer(Renderer& p_renderer);
	~LayerRenderer();

	//todo: I guess some kind of identifier to identify the layers?
	LayerIndex AddLayer(); //returns layerIndex.
	void RemoveLayer(LayerIndex layerIndex); //Should probably never have to use this but whatever.

	//std::shared_ptr<Layer> GetLayer(LayerIndex layerIndex); //Incase they want to add something to a layer.

	void RenderLayers();

private:
	std::vector<std::shared_ptr<Layer> > layers;
	Renderer& renderer;

};