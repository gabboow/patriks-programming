#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class BasicShader
{
public:
	BasicShader();
	~BasicShader();

	GLuint& GetShaderID();

private:
	const char* vshCode;
	const char* fshCode;
	GLuint programID;
};

