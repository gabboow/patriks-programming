#include "Window.h"
#include "Engine.h"

#include <iostream>

Window::Window(const uint32_t& p_width, const uint32_t& p_height) :
	width {p_width},
	height { p_height},
	_window{ glfwCreateWindow(p_width, p_height, "tower defense", NULL, NULL)  }
{
		if (!_window.get() ) {
		glfwTerminate();
		std::cout << "Failed getting window. \n";
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(_window.get()); //after this call i can init glew.

	glfwSetFramebufferSizeCallback(_window.get(), FrameBufferCallback);
	glfwSetWindowSizeCallback(_window.get(),WindowCallback);
}
Window::~Window() {
	//Window::instance = nullptr;
}


//for window resizing.
void Window::WindowCallback(GLFWwindow * p_window, int p_width, int p_height) {
	//todo: and what do we do in here?
	Engine* engine = static_cast<Engine*>(glfwGetWindowUserPointer(p_window));
	Window* window = engine->windowPointer;


	//todo: and now We can call the window resize callback!

	std::cout << "Window Resized!" << '\n';
	std::cout << "Width: " << p_width << '\n';
	std::cout << "Height: " << p_height << '\n';

	//glfwGetFramebufferSize(p_window,)//todo: this should be the same as the windowCallbacxks p_width, p_height, if not, we should use this function instead.
	window->onWindowResizeCallback();
}

void Window::FrameBufferCallback(GLFWwindow* p_window, int p_width, int p_height) {

	int xPos, yPos;
	glfwGetWindowPos(p_window, &xPos, &yPos);
	glfwSetWindowPos(p_window, xPos, yPos);
	glfwSetWindowSize(p_window, p_width, p_height);
	glViewport(0, 0, p_width, p_height);
}




void Window::OnWindowResize(std::function< void() > p_callback) {
	onWindowResizeCallback = p_callback;
}



GLFWwindow* Window::GetWindow()
{
	return _window.get();
}

uint32_t& Window::GetWidth() {
	int width, height;
	glfwGetWindowSize(_window.get(), &width, &height);
	uint32_t returnedWidth = (uint32_t)width;
	return returnedWidth;
}

uint32_t& Window::GetHeight() {
	int width, height;
	glfwGetWindowSize(_window.get(), &width, &height);
	uint32_t returnedHeight = (uint32_t)height;
	return returnedHeight;
}

//todo: send in to states, so that we may be able to get screensize from in there.