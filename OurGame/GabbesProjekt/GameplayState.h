#include "IState.h"

#include "Renderer.h"
#include "Layer.h"

//#include <entt/entity/registry.hpp>
#include <entt/entt.hpp>

class GameplayState : public IState {

public:

	GameplayState(Renderer& p_renderer);
	~GameplayState();
	void Enter();
	void Exit();
	void Update(float p_deltaTime);
	void Draw();

private:
	Renderer& renderer;
	std::vector<Layer> layers;
	entt::registry registry;

};