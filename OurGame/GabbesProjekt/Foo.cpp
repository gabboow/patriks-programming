#include "Foo.h"

Foo::Foo(std::string p_name) : name{ p_name } {
	std::cout << name << "is bourne." << '\n';
}

Foo::~Foo() {
	std::cout << "I am destroyed " << name << '\n';
}

int Foo::Add(int x, int y) {
	return x + y;
}

void Foo::SmackMyBitchUp(std::string myBitch) {
	std::cout << myBitch << " is beautiful! \n" << '\n';
}

void Foo::SetName(std::string p_name) {
	name = p_name;
}