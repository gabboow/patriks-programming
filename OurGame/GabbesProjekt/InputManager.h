#pragma once



#include "GL/glew.h"
#include "GLFW/glfw3.h"



#include "Window.h"



#include <map>
class Engine;

enum class ButtonState
{
	PRESSED,
	HOLD,
	RELEASED,
};

enum class ScrollState {
	UP,
	DOWN,
	NOTHING
};


class InputManager
{
public:
	InputManager(Window& p_window);
	~InputManager();
	//move constructor
	InputManager(InputManager&& p_other) = delete;
	//copy assignment operator
	InputManager& operator=(const InputManager& p_other) = delete;
	//copy constructor.
	InputManager(const InputManager& p_other) = delete;

	bool RegisterKeyInput(int p_scancode);



	bool IsKeyDown(int p_key);
	bool IsKeyDownOnce(int p_key);

	bool IsMouseButtonDown(int p_key);
	bool IsMouseButtonDownOnce(int p_key);

	bool IsMouseScrollUp();
	bool IsMouseScrollDown();

	void Update(); //Todo: this is only for updating the scroll to nothing after frame is done, basically it is nearly useless.

	//todo: mouse input.


private:
	bool FindKey(int p_key);

	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void MouseCallback(GLFWwindow* p_window, int p_button, int p_action, int p_mods);
	static void ScrollCallback(GLFWwindow* p_window, double xoffset, double yoffset);

	//std::vector<Key> keys; //Todo: change into map.
	std::map<int, ButtonState> keys;
	std::map<int, ButtonState> mouseButtons;
	ScrollState scrollState;

	Window& window;

};

