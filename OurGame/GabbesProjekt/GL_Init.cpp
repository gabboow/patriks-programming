#include "GL_Init.h"
#include <iostream>


GL_Init::GL_Init()
{
}

GL_Init::~GL_Init()
{
	glfwTerminate();
}

void GL_Init::InitGLFW()
{
	glfwSetErrorCallback(GL_Init::ErrorCallback);
	
	if (glfwInit() == GLFW_FALSE)
	{
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
}

void GL_Init::InitGLEW()
{
	if (glewInit() != GLEW_OK) {
	std::cout << "GLEW FAILED." << '\n';
	exit(EXIT_FAILURE);
	}
}

/** For GLFW only, does not work for GLEW errors.*/
void GL_Init::ErrorCallback(int p_int, const char* p_err) {
	std::cout << "GLFW ERROR: " << p_err << '\n';
}