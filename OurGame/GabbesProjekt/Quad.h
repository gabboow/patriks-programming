#pragma once

#include "Vertex.h"
#include <array>
#include "TextureShader.h"
#include "Texture.h"
#include "Layer.h"

//deprecated, ok to remove.
struct Quad {
	Quad(std::array<Vertex, 4> p_vertices) : 
		vertices{ p_vertices },
		layerIndex{0}
	{}

	Quad(const Quad&) = default; //default as we want shallow copies. maybe not for position tho? idk.
	//assignment constructor.
	//destructor.
	std::array<Vertex,4> vertices;
	//could be changed into some kind of id instead i guess.
	//unsigned int layerIndex; //what layer we render to.
private:
	unsigned int layerIndex = 0; //only to be accessed by layers.
	friend class Layer;
};
