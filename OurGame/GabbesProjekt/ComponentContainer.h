#pragma once
#include<vector>

template <typename T, typename... Rest>
class ComponentContainer
{
	ComponentContainer() = default;
	ComponentContainer(size_t p_numberOfComponents, const T& first, const Rest& ... rest) :
		numberOfComponents{ p_numberOfComponents },
		currentComponent(first),
		rest(rest...) { };
	{

	}
	std::vector<T> currentComponent; //
	ComponentContainer<Rest ... > rest;
	size_t numberOfComponents;


};