#pragma once

#include <string>
#include "mat4.h"

struct Vector2
{

public:



	Vector2(const Vector2&) = default;
	Vector2();


	Vector2(float p_x, float p_y);


	float x;
	float y;

	float Length(); //l�ngden mellan x och y.
	static Vector2 DistanceTo(const Vector2& p_vecA, const Vector2& p_vecB); //avst�ndet mellan vektor A och vektor B
	Vector2 Normalize(); //normaliserad version av vektor2
	static float Dot(const Vector2& p_vecA, const Vector2& p_vecB); //dotprodukten av vec A och B
	void PrintVector(std::string p_vectorName = "vector");

	Vector2& operator+=(const Vector2& p_vec);
	Vector2& operator-=(const Vector2& p_vec);
	Vector2& operator*=(const Vector2& p_vec);
	Vector2& operator*(const Vector2& p_vec);
	Vector2& operator+(const Vector2& p_vec);
	Vector2& operator-(const Vector2& p_vec);

};




