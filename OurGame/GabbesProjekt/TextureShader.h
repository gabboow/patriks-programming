#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <vector>
#include "Vertex.h"

#include "IShader.h"
#include "mat4.h"

class TextureShader : public IShader {
public:
	TextureShader();
	~TextureShader();

	GLuint& GetShaderID() override;

	void UpdateVertices() override;
	void Bind() override;

	void SetUniform(const std::string& p_uniform, float p_value) override;
	void SetUniform(const std::string& p_uniform, int p_value) override;

	void SetUniformMat4(const std::string& p_uniform, mat4& mat);

private:

	const char* vshCode;
	const char* fshCode;
	GLuint programID;
};

