#pragma once
#include <GL/glew.h>
#include "GL_Init.h"
#include "InputManager.h"

class Window;
class Camera;

class Engine
{
public:
	Engine();
	~Engine();

	/** These are stored here so that we can access them from the setUserWindowPointer thingie.*/
	InputManager* inputManagerPointer;
	Window* windowPointer; //stored as pointers, initially created on stack.
	Camera* cameraPointer;


	void Run();

	void OnResize();

private:

};

