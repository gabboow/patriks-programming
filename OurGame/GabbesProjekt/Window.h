#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <functional>
class Engine;


struct DestroyglfwWin {
	void operator()(GLFWwindow* ptr) {
		glfwDestroyWindow(ptr);
	}
};

class Window {
public:
	Window(const uint32_t& width, const uint32_t& height);
	~Window();
	GLFWwindow* GetWindow();

	uint32_t& GetWidth();
	uint32_t& GetHeight();

	static void WindowCallback(GLFWwindow* p_window,int p, int p2);
	static void FrameBufferCallback(GLFWwindow* p_window, int p, int p2);
	
	void OnWindowResize(std::function< void() > p_callback); //todo: pass in function pointer to here i guess, have it be called when we resize?


	/**Todo: figure out something better than public members for this lol. */
	std::function<void()> onWindowResizeCallback;


private:

	uint32_t width;
	uint32_t height;
	std::unique_ptr<GLFWwindow, DestroyglfwWin> _window;
};
