#pragma once

#include "Texture.h"
#include "TextureShader.h" //todo: later on allow for different shaders.

//shader + texture = material.
class Material
{
public:
	Material(Texture* p_texture, TextureShader* p_shader); //shared ptr
	~Material();
	//move constructor
	Material(Material&& p_other) = delete;
	//copy assignment operator
	Material& operator=(const Material& p_other) = delete;
	//copy constructor.
	Material(const Material& p_other) = delete;
	void Bind();
	TextureShader* GetShader();
	Texture* GetTexture();
private:
	Texture* texture;
	TextureShader* shader;
};

