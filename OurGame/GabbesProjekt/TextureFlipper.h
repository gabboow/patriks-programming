#pragma once

class TextureFlipper


{
public:
	TextureFlipper();
	~TextureFlipper();

	void FlipTexture(unsigned char* p_bytes, int p_width, int p_height, int p_channels);
};

