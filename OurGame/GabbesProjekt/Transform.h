#pragma once
#include "Vector2.h"

#include "mat4.h"

struct Transform {
	Transform() : /*position{ 0.0f,0.0f }, scale{ 1.0f,1.0f }, rotation{ 0.0f },*/ matrix{identity()}
	{};

	Transform(const Transform&) = default;

	//Vector2 position;
	//Vector2 scale;
	//float rotation;

	mat4 matrix;

};