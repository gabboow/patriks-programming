#pragma once

#include "Vector2.h"
#include "mat4.h"

class Camera
{
public:

	//todo: zoom function.

	Camera(float left, float right, float bottom, float top, float aspectRatio);


	const Vector2& GetPosition() const { return position; }
	const float& GetRotation() const { return rotation; }

	void SetPosition(const Vector2& p_position) { position = p_position; RecalculateViewMatrix(); }
	void SetRotation(float p_rotation) { rotation = p_rotation; RecalculateViewMatrix(); }

	void Zoom(float p_value);

	const mat4& GetProjectionMatrix() const { return projectionMatrix; }
	const mat4 GetViewMatrix() const { return viewMatrix; }
	mat4& GetViewProjectionMatrix() { return viewProjectionMatrix; }


	/**Updating aspect ratio on resize.*/
	void SetAspectRatio(float p_value);

private:
	void RecalculateViewMatrix();
	void RecalculateProjectionMatrix();


	Vector2 position;

	mat4 projectionMatrix;
	mat4 viewMatrix;
	mat4 viewProjectionMatrix;


	float aspectRatio;
	float rotation;
	
	float zoomValue;
	float left;
	float right;
	float up;
	float down;

};

