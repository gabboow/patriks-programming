#pragma once
#include <string>
#include <iostream>
#include <chrono>
class Logger
{public:
	Logger(std::string p_LoggerInfo);
	~Logger();

	std::string loggerInfo;

private:
	std::chrono::steady_clock::time_point constructTime;
	float totalTime;
};

