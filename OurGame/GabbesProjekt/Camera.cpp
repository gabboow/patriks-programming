#include "Camera.h"
#include <iostream>

//https://www.youtube.com/watch?v=NjKv-HWstxA

Camera::Camera(float p_left, float p_right, float p_bottom, float p_top,float p_aspectRatio) :
	position{ 0.0f,0.0f },
	rotation{0.0f},
	zoomValue{1.0f},
	up{p_top},
	down{p_bottom},
	left{p_left},
	right{p_right},
	aspectRatio{p_aspectRatio},
	viewMatrix{ identity() }
 {
	float xSpan = 1.0f;
	float ySpan = 1.0f;
	if (p_aspectRatio > 1.0f) {
		xSpan *= aspectRatio;
	}
	else {
		ySpan = xSpan / aspectRatio;
	}
	projectionMatrix = { ortho(p_left * xSpan,p_right*xSpan,p_bottom*ySpan,p_top*ySpan,-1.0f,1.0f) },
	viewProjectionMatrix = projectionMatrix * viewMatrix;

}

void Camera::SetAspectRatio(float p_value) {

	aspectRatio = p_value;
	RecalculateProjectionMatrix();
}

void Camera::RecalculateProjectionMatrix() {
	float xSpan = 1.0f;
	float ySpan = 1.0f;
	if (aspectRatio > 1.0f) {
		xSpan *= aspectRatio;
	}
	else {
		ySpan = xSpan / aspectRatio;
	}
	projectionMatrix = ortho(left * xSpan * zoomValue, right * xSpan * zoomValue, down * ySpan * zoomValue, up * ySpan * zoomValue, -1.0f, 1.0f);
	viewProjectionMatrix = projectionMatrix * viewMatrix;
}

//todo: setup shader uniform for matrix.

void Camera::RecalculateViewMatrix() {

	mat4 transform = translation(position.x, position.y, 0.0f) * rotationZ((rotation * (float)PI) / 180.0f);
	viewMatrix = invertMatrix(transform);

	viewProjectionMatrix = projectionMatrix * viewMatrix;

}

void Camera::Zoom(float p_value) {
	zoomValue += p_value;

	projectionMatrix = ortho( left * aspectRatio * zoomValue,right * aspectRatio * zoomValue ,down * zoomValue,up * zoomValue,-1.0f,1.0f );
	RecalculateViewMatrix();
}
