#pragma once

#include "Window.h"
#pragma warning(push, 0)
#pragma warning (disable: 26495 6011)
#include "External/imgui/imgui.h"
#include "External/imgui/imgui_impl_opengl3.h"
#include "External/imgui/imgui_impl_glfw.h"
#pragma warning( pop )


class Editor
{
	//todo: Here we will do all of the editor (imgui) based stuff.
	//Such as:
		//loading stuff.
		//Moving around and populating a scene.
		//saving stuff.
		//editing properties such as shader properties.
		//Later on we'll have a units tab
		//and an environment tab
		//and other cool shit.

public :

	Editor(Window& p_window);
	~Editor();
	void Update();
	void Render();

private:
	float f = 0.0f;
	ImVec4 color = { 0.0f,0.0f,0.0f,0.0f };


};

