#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class GL_Init
{
public:
	GL_Init();
	~GL_Init();

	void InitGLFW();
	void InitGLEW();

	static void ErrorCallback(int p_int, const char* p_err);

private:

};

