#include "TextureFlipper.h"
#include <iostream>
#include <cmath>

TextureFlipper::TextureFlipper()
{
}

TextureFlipper::~TextureFlipper()
{
}

void TextureFlipper::FlipTexture(unsigned char* p_bytes, int p_width, int p_height, int p_channels)
{
    int textureSize=p_width* p_height* p_channels;

    int j = 0;
    for (j = 0; j*2 < p_height; ++j) {
        unsigned char topRow = j * p_width * p_channels;
        unsigned char botRow = (p_height - 1 - j) * p_width * p_channels;
        for (int i = p_width * p_channels; i > 0 ; --i) {
            unsigned char temp = p_bytes[botRow];
            p_bytes[botRow] = p_bytes[topRow];
            p_bytes[topRow] = temp;
            ++topRow;
            ++botRow;
        }
	}
    std::cout << p_bytes << "\n" << "flipped texture has " << j <<" entries. \n";
}
