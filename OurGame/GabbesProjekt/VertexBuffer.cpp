#include "VertexBuffer.h"
#include "Vertex.h"

VertexBuffer::VertexBuffer(const unsigned int& P_MAX_VERTEX_COUNT) {
	glGenBuffers(1, &VB);
	glBindBuffer(GL_ARRAY_BUFFER, VB);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * P_MAX_VERTEX_COUNT, nullptr, GL_DYNAMIC_DRAW);
}

VertexBuffer::~VertexBuffer() {
	glDeleteBuffers(1, &VB);
}

void VertexBuffer::Bind(const size_t& p_count,const void* p_data) {
	glBindBuffer(GL_ARRAY_BUFFER, VB);
	glBufferSubData(GL_ARRAY_BUFFER, 0, p_count * sizeof(Vertex) , p_data );
}