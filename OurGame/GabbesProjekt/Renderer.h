#pragma once
#include "Vertex.h"
#include <array>
#include "Texture.h"
#include "TextureShader.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Material.h"
#include <entt/entity/registry.hpp>
#include "Vector2.h"
#include "mat4.h"
#include "Transform.h"

class Renderer
{
public:
	Renderer(mat4& p_cameraProjectionViewMatrix);
	~Renderer();
	//move constructor
	Renderer(Renderer&& p_other) = delete;
	//copy assignment operator
	Renderer& operator=(const Renderer& p_other) = delete;
	//Deleting copy constructor.
	Renderer(const Renderer& p_other) = delete;

	
	void SetRegistry(entt::registry* p_registry);
	void BeginDrawCall(Material* p_material);
	//void RenderQuad(float p_atlasPosition, float p_scale, Vector2 p_position);
	void RenderQuad(Transform& p_transform, float p_atlasPosition);
	void EndDrawCall(TextureShader* p_shader);
	void SortLayers();
	void ResetDrawCalls();

private:

	std::vector<GLuint> CreateIndices(unsigned int p_count);


	//void Render(TextureShader& p_shader, Texture& p_texture);
	Material* currentMaterial;
	mat4& cameraProjectionViewMatrix;

	VertexArray vertexArray;
	VertexBuffer vertexBuffer;
	IndexBuffer indexBuffer;
	
	entt::registry* registry;
	Vertex* buffer; //really another vertexBuffer, but on the cpu. "vertexData"
	Vertex* bufferPointer;
	unsigned int bufferSize;
	unsigned int quadCount;

	unsigned int drawCallCount;
	

};

