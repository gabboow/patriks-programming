#include "GameplayState.h"

#include <iostream>

#include "TextureShader.h"
#include "Texture.h"

#include "Renderable.h"
#include "Material.h"
#include "MaterialManager.h"

#include "Transform.h"

GameplayState::GameplayState(Renderer& p_renderer) : 
	renderer{p_renderer},
	registry{}
{ 

}

GameplayState::~GameplayState()
{
}

void GameplayState::Enter()
{
	renderer.SetRegistry(&registry);

	Layer testLayer = { renderer };
	//Texture* texture = new Texture("../Assets/T_Texture_Atlas.png"); //todo: shared ptr.
	Texture* texture = new Texture("../Assets/4kTest.jpg"); //todo: shared ptr.

	TextureShader* textureShader = new TextureShader(); //todo: shared ptr.
	textureShader->Bind();
	
	textureShader->SetUniform("vGridSize", 2000.0f);
	textureShader->SetUniform("vAtlasWidth", 4000.0f);
	textureShader->SetUniform("vAtlasHeight", 4000.0f);
	//what happens if we bind something else and set uniforms? (we'd have to reset it assuming we are using same shader)

	Material* material = new Material(texture, textureShader);//so maybe we'll just store that info in the material, that way materials can make use of same shader.
	//todo: a setUniform onBind function, so when material gets bound we set shaderUniforms.
	//then all materials would have to be uniform size though, might be better to store in renderable and just set it for each entity instead?
	//since this is so boilerplatey code that We'll use gridsize,atlasWidth and altasHeight for each entity, we'll go with this solution.

	for (int i = 0; i < 100; i++) {
		auto entity = registry.create();

		unsigned int atlasIndex = i % 4;
		registry.emplace<Renderable>(entity,material,atlasIndex);
		auto& transform = registry.emplace<Transform>(entity);

		transform.matrix = transform.matrix * translation(1.0f + 1.0f*i, 0.0f, 0.0f);
		transform.matrix = transform.matrix * identity(); 
		transform.matrix = transform.matrix * scale(1.0f);

		testLayer.AddEntity(registry, entity);
	}
	layers.push_back(testLayer);
}

void GameplayState::Exit()
{
	//todo: temp cleanup. but should use shared pointers so we don't have to.
}

void GameplayState::Update(float p_deltaTime)
{

}

void GameplayState::Draw() {
	renderer.SortLayers();
	for (int i = 0; i < layers.size(); i++) {
		layers[i].Render(registry);
	}
	renderer.ResetDrawCalls();
}
