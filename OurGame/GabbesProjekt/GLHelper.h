#pragma once



#include <GL/glew.h>


#include <cstdio>
#include <assert.h>
#include <iostream>
//include glew.


static void _DumpGLErrors(const char* file, int line) {
	line--;
	GLenum error = glGetError();
	switch (error) {
	case GL_INVALID_ENUM: printf("GL ERROR: GL_INVALID_ENUM @file: %s @line:%d\n", file, line); break;
	case GL_INVALID_VALUE: printf("GL ERROR: GL_INVALID_VALUE @file: %s @line:%d\n", file, line); break;
	case GL_INVALID_OPERATION: printf("GL ERROR: GL_INVALID_OPERATION @file: %s @line:%d\n", file, line); break;
	case GL_INVALID_FRAMEBUFFER_OPERATION: printf("GL ERROR: GL_INVALID_FRAMEBUFFER_OPERATION @file: %s @line:%d\n", file, line); break;
	case GL_OUT_OF_MEMORY: printf("GL ERROR: GL_OUT_OF_MEMORY @file: %s @line:%d\n", file, line); break;
	case GL_NO_ERROR:
		// printf("GL_NO_ERROR @ %s %s %d\n", proc, file, line); break;
	default:
		break;
	}
}

#define DumpGLErrors() _DumpGLErrors(__FILE__, __LINE__)


/** Taken from some old course i did.*/
static GLuint CreateProgram(const char* vshCode, const char* fshCode) {

	// COMPILE VERTEX SHADER
	GLuint vshID = glCreateShader(GL_VERTEX_SHADER);
	DumpGLErrors();
	assert(glIsShader(vshID));
	glShaderSource(vshID, 1, (const char**)&vshCode, 0); //todo: 
	DumpGLErrors();
	glCompileShader(vshID);
	DumpGLErrors();
	GLint vshIsCompiled;

	glGetShaderiv(vshID, GL_COMPILE_STATUS, &vshIsCompiled);
	DumpGLErrors();
	if (!vshIsCompiled) {
		GLsizei length;
		char buffer[4096];
		glGetShaderInfoLog(vshID, sizeof(buffer), &length, buffer);
		DumpGLErrors();
		if (length > 0) printf("Vertex shader compile errors: \n%s\n", buffer);
		assert(!"Vertex shader compilation failed!");
	}

	// COMPILE FRAGMENT SHADER
	GLuint fshID = glCreateShader(GL_FRAGMENT_SHADER);
	DumpGLErrors();
	glShaderSource(fshID, 1, (const char**)&fshCode, 0); //todo: glChar?
	DumpGLErrors();
	assert(glIsShader(fshID));
	glCompileShader(fshID);
	DumpGLErrors();
	GLint fshIsCompiled;
	glGetShaderiv(fshID, GL_COMPILE_STATUS, &fshIsCompiled);
	DumpGLErrors();
	if (!fshIsCompiled) {
		GLsizei length;
		char buffer[4096];
		glGetShaderInfoLog(fshID, sizeof(buffer), &length, buffer);
		DumpGLErrors();
		if (length > 0) printf("Fragment shader compile errors: \n%s\n", buffer);
		assert(!"Fragment shader compilation failed!");
	}

	// LINK
	GLuint programID = glCreateProgram();
	assert(glIsProgram(programID));
	glAttachShader(programID, vshID);
	glAttachShader(programID, fshID);
	glLinkProgram(programID);

	glValidateProgram(programID);
	GLint isLinked;
	glGetProgramiv(programID, GL_LINK_STATUS, &isLinked);

	if (!isLinked) {
		GLsizei length;
		char buffer[4096];
		glGetProgramInfoLog(programID, sizeof(buffer), &length, buffer);
		if (length > 0) printf("Program link errors: \n%s\n", buffer);
		assert(!"Program linking failed");
	}

	glDeleteShader(vshID);
	glDeleteShader(fshID);

	return programID;
}