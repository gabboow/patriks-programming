#include <math.h>
#include <iostream>
#include "Vector2.h"

Vector2::Vector2(float p_x , float p_y) 
    : x{ p_x },
      y{ p_y }
{
}

Vector2::Vector2() : x{ 0.0f }, y{0.0f} {

}


float Vector2::Length()
{
    float length = sqrt (pow (x,2) + pow (y,2));
    return length;
}

Vector2 Vector2::DistanceTo(const Vector2& p_vecA, const Vector2& p_vecB)
{
    Vector2 distanceAB;
    distanceAB.x = abs (p_vecB.x - p_vecA.x);
    distanceAB.y = abs (p_vecB.y - p_vecA.y);
    return Vector2(distanceAB);
}

Vector2 Vector2::Normalize()
{
    Vector2 normalized;
    if (Length() != 0)
    {
         normalized.x = x / Length();
         normalized.y = y / Length();
     }
    else
    {
         normalized.x = 0;
         normalized.y = 0;
    }
    return normalized;
}

float Vector2::Dot(const Vector2& p_vecA, const Vector2& p_vecB)
{
    return p_vecA.x * p_vecB.x + p_vecA.y * p_vecB.y;
}

void Vector2::PrintVector(std::string p_vectorName)
{
    std::cout << p_vectorName << " = " << x << "," << y << "\n";
}

Vector2& Vector2::operator+=(const Vector2& p_vec)
{
    x += p_vec.x;
    y += p_vec.y;
    return *this;
}

Vector2& Vector2::operator-=(const Vector2& p_vec)
{
    x -= p_vec.x;
    y -= p_vec.y;
    return *this;
}

Vector2& Vector2::operator*=(const Vector2& p_vec)
{
    x *= p_vec.x;
    y *= p_vec.y;
    return *this;
}

Vector2& Vector2::operator*(const Vector2& p_vec)
{
   // Vector2 result;
    x = x * p_vec.x;
    y = y * p_vec.y;
    return *this;
}

Vector2& Vector2::operator+(const Vector2& p_vec)
{
   // Vector2 result;
    x = x + p_vec.x;
    y = y + p_vec.y;
    return *this;
}

Vector2& Vector2::operator-(const Vector2& p_vec)
{
    //Vector2 result;
    x = x - p_vec.x;
    y = y - p_vec.y;
    return *this;
}



