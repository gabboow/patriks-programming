#pragma once
#include <map>
#include <memory>
#include "Material.h"

class MaterialManager
{
public:
	MaterialManager();
	~MaterialManager();

														/* TextureShader should obviously be replaced with some kind of baseclass.  */
	void AddMaterial(std::string p_key, Texture* p_texture,TextureShader* p_shader);
	std::shared_ptr<Material> GetMaterial(std::string p_key);

private:
	std::map<std::string, std::shared_ptr<Material> > materials;

};

