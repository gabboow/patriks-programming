#include "Layer.h"
#include "Renderable.h"
#include <cassert>
#include "Transform.h"

//Add material manager, basically we need materials lol.
Layer::Layer(Renderer& p_renderer) : 
	renderer{p_renderer},
	layerIndex{layerCount}
{
	layerCount++;
}

Layer::~Layer() {
	//todo: remove stuff.
}


void Layer::AddMaterial(Material* p_material) {
	//Add material as key, if it doesn't already exist.
	for (int i = 0; i < drawCallGroup.size(); i++) {
		if (drawCallGroup[i] == p_material) {
			return;
		}
	}
	drawCallGroup.push_back(p_material);
}

//todo: something better than passing in a material* i guess.
//also kind of suspect that we decide what material to use like this.
//if we do a material manager, we can have an enum that keys -> enum to material.
void Layer::AddEntity(entt::registry& p_registry, entt::entity p_entity) {
	assert(p_registry.any<Renderable>(p_entity) && "Entity did not have a Renderable component when adding it to a layer");
	assert(p_registry.any<Transform>(p_entity) && "Entity did not have a Transform component when adding it to a layer");

	//todo: entity should also have a transform so that we may set position.
	auto& renderable = p_registry.get<Renderable>(p_entity);
	AddMaterial(renderable.material);
	renderable.layerIndex = layerIndex;
	for (int i = 0; i < drawCallGroup.size(); i++) {
		if (drawCallGroup[i] == renderable.material) { //todo: if this never happens, assert.
			renderable.drawCallGroup = i;
		}
	}
}

void Layer::Render(entt::registry& p_registry) { //TODO: GET CAMERA VIEWPROJECTIONMATRIX IN HERE.
	auto view = p_registry.view<Renderable,Transform>();
	for (int i = 0; i < drawCallGroup.size(); i++) {
		renderer.BeginDrawCall(drawCallGroup[i]);
		for (auto entity : view) {
			auto& renderable = p_registry.get<Renderable>(entity); 
			auto& transform = p_registry.get<Transform>(entity);

			if (renderable.drawCallGroup == i) { //Todo: should use scale not just scale.x
				renderer.RenderQuad(transform,(float)renderable.atlasIndex); 
			}
			else {
				break; //so we don't have to loop over every entity, assumes they are sorted though.
			}
		}
		renderer.EndDrawCall(drawCallGroup[i]->GetShader());
	}
}