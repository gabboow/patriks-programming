#pragma once
#include <string>
#include <GL/glew.h>

class Texture
{
public:

	Texture(const std::string& p_filePath);
	~Texture();

	//move constructor
	Texture(Texture&& p_other) = delete;
	//copy assignment operator
	Texture& operator=(const Texture& p_other) = delete;
	//copy constructor.
	Texture(const Texture& p_other) = delete;

	void Bind();

private:
	GLuint textureID;

};

