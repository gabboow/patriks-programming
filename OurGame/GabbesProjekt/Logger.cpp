#include "Logger.h"



Logger::Logger(std::string p_LoggerInfo)
{
	loggerInfo = p_LoggerInfo;
	constructTime = std::chrono::high_resolution_clock::now();
}

Logger::~Logger()
{
	std::chrono::steady_clock::time_point destructTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float, std::milli> timeSinceConstruct = destructTime - constructTime;
	std::cout << loggerInfo << timeSinceConstruct.count() << '\n';
}
