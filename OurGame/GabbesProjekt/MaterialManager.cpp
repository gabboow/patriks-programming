#include "MaterialManager.h"
#include <cassert>

MaterialManager::MaterialManager() { 
}

MaterialManager::~MaterialManager() {
}
																		//obviously replace with a base class later on, also shared_ptr
void MaterialManager::AddMaterial(std::string p_key, Texture* p_texture, TextureShader* p_shader) {
	std::shared_ptr material = std::make_shared<Material>(p_texture, p_shader); //args
	materials.insert(std::pair<std::string, std::shared_ptr<Material> >(p_key, material));
}

std::shared_ptr<Material> MaterialManager::GetMaterial(std::string p_key) {
	auto it = materials.find(p_key);
	assert(it != materials.end());

	return it->second;
}

