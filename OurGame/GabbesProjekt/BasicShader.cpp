#include "BasicShader.h"
#include "GLHelper.h"

BasicShader::BasicShader() : vshCode{ R"BLAH( 
	#version 330
	in vec3 vPositions;
	in vec3 vColors;
	out vec4 fColors;
	uniform float vTime;
	out float fTime;
	void main() {

	fColors= vec4(vColors.x,vColors.y,vColors.z, 1);
	gl_Position=vec4(vPositions,1);
	fTime = vTime;

	}
)BLAH" },
fshCode{ R"Blah(
	#version 330
	in vec4 fColors;
	in float fTime;
	out vec4 rColors;

	void main () {
	//float sinTime = (sin( fTime*0.002 )* 0.5) + 0.5; 
	rColors = fColors;//vec4(fract(sin(fColors*fTime)*100000));
	}
)Blah"
}
{
	programID = CreateProgram(vshCode, fshCode);
}

BasicShader::~BasicShader()
{
	glDeleteProgram(programID);
	delete vshCode;
	vshCode = nullptr;
	delete fshCode;
	fshCode = nullptr;
}

GLuint& BasicShader::GetShaderID()
{
	return programID;
}
