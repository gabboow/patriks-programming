#pragma once
#include "TextureShader.h"
#include "Texture.h"
#include "Renderer.h"

#include <map>
#include <vector>
#include <entt/entity/registry.hpp>
#include "LayerTypes.h"
#include "mat4.h"


static unsigned int layerCount = 0;
class Layer
{
public:

	Layer(Renderer& p_renderer);
	~Layer();

	/**Temporary functions, until we have an actual entity defined. */

	void AddEntity(entt::registry& , entt::entity); //todo: this.
	void Render(entt::registry&);

private:

	void AddMaterial(Material*); //todo: should def be shared ptr WHY? stop being a pussy.

	LayerIndex layerIndex;
	Renderer& renderer;

	std::vector<Material*> drawCallGroup; //todo:
	//std::vector<Texture*> textureVec; 
};

