#include "Editor.h"

//supressing external stuff warnings.


Editor::Editor(Window& p_window) {
	ImGui::CreateContext();
	ImGui_ImplGlfw_InitForOpenGL(p_window.GetWindow(), true);
	ImGui_ImplOpenGL3_Init();
	ImGui::StyleColorsDark();
}

Editor::~Editor() {
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

void Editor::Update() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();

	ImGui::NewFrame();
	//todo: I guess something useful. such as saving a scene / loading it.

	ImGui::Begin("Editor");
	ImGui::Text("Description text here.");
	ImGui::ColorEdit3("clear color", (float*)&color);

	ImGui::SliderFloat("float", &f, 0.0f, 1.0f);

	ImGui::End();

}

void Editor::Render() {
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

