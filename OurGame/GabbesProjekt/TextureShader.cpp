#include "TextureShader.h"
#include "BasicShader.h"
#include "GLHelper.h"

TextureShader::TextureShader() : vshCode{ R"BLAH( 
	#version 330
	in float vAtlasPosition;
	uniform float vGridSize;
	uniform float vAtlasWidth;
	uniform float vAtlasHeight;

	uniform mat4 viewProjection;
	

	in vec3 vPositions;
	in vec2 vUV;
	in vec3 vColors;
	out vec4 fColors;
	out vec2 fUV;

	out float fAtlasPosition;
	out float fGridSize;
	out float fAtlasWidth;
	out float fAtlasHeight;

	void main() {
	
    fAtlasPosition = vAtlasPosition;
	fGridSize = vGridSize;
	fAtlasWidth = vAtlasWidth;
	fAtlasHeight = vAtlasHeight;

	fUV = vUV;
	fColors= vec4(vColors.x,vColors.y,vColors.z, 1);
	gl_Position = viewProjection * vec4(vPositions,1);


	}
)BLAH" },
fshCode{ R"Blah(
	#version 330
	in vec4 fColors;
	in vec2 fUV;
	in float fGridSize;
	in float fAtlasPosition;
	in float fAtlasWidth;
	in float fAtlasHeight;

	out vec4 rColors;
	uniform sampler2D ourTexture;

	void main () {

	float textureWidth = (fGridSize/fAtlasWidth); // 128 / 256 = 0.5.
    float textureHeight = (fGridSize/fAtlasHeight); // 128 / 256 = 0.5.
    float gridWidth = (fAtlasWidth / fGridSize); // 256 / 126 = 2.
    float gridHeight = (fAtlasHeight / fGridSize); // 256 / 126 = 2

    float atlasXPos = mod(fAtlasPosition,gridWidth); // 1 % 2 = 1.
    float atlasYPos = floor(fAtlasPosition / gridHeight); // floor(1 / 2) = 0.
    float currentTexturePosX = textureWidth * fUV.x + (atlasXPos * textureWidth); //0.5 * (0 -> 1) + (1 * 0.5) = 0.5 -> 1.
    float currentTexturePosY = textureHeight * fUV.y + (atlasYPos *textureHeight); //0.5 * (0 -> 1) + (0 * 0.5) = 0 -> 0.5
    vec2 texturePosition = vec2(currentTexturePosX, currentTexturePosY);

    vec4 sum = texture2D(ourTexture, texturePosition);
    rColors = sum;
	
	}
)Blah"
}
{
	programID = CreateProgram(vshCode, fshCode);
}


/**
(our indexBuffer, vertexBuffer etc already has the data bound to GPU, now we're just telling the GPU how to read the data.)
*/
void TextureShader::UpdateVertices() {

	GLint positions = glGetAttribLocation(programID, "vPositions");
	glVertexAttribPointer(positions, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, positions));
	glEnableVertexAttribArray(positions);

	GLint colors = glGetAttribLocation(programID, "vColors");
	glVertexAttribPointer(colors, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, colors));
	glEnableVertexAttribArray(colors);

	GLint uvs = glGetAttribLocation(programID, "vUV");
	glVertexAttribPointer(uvs, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, uvs));
	glEnableVertexAttribArray(uvs);

	GLint atlasP = glGetAttribLocation(programID, "vAtlasPosition");
	glVertexAttribPointer(atlasP, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, atlasPosition));
	glEnableVertexAttribArray(atlasP);

	//todo: set viewProjection here I guess?

}


//TODO: SET THIS SHIT SOMEWHERE.
void TextureShader::SetUniformMat4(const std::string& p_uniform,mat4& p_mat) {
	GLint P = glGetUniformLocation(programID, p_uniform.c_str());

	if (P != -1) {
		glUniformMatrix4fv(P, 1, GL_FALSE, p_mat.m);
	}
}

void TextureShader::SetUniform(const std::string& p_uniform, float p_value) {
	GLint P = glGetUniformLocation(programID, p_uniform.c_str() );
	if (P != -1)
	{
		glUniform1f(P, p_value);
	}
}

void TextureShader::SetUniform(const std::string& p_uniform, int p_value) {
	GLint P = glGetUniformLocation(programID, p_uniform.c_str() );
	if (P != -1)
	{
		glUniform1i(P, p_value);
	}
}

TextureShader::~TextureShader()
{
	glDeleteProgram(programID);
	delete vshCode;
	vshCode = nullptr;
	delete fshCode;
	fshCode = nullptr;
}

GLuint& TextureShader::GetShaderID()
{
	return programID;
}


void TextureShader::Bind() {
	glUseProgram(programID);
}
