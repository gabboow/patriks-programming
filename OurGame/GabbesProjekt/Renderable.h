#pragma once
#include "Layer.h"
#include "Renderer.h"
#include "Material.h"

struct Renderable {

	Renderable(Material* p_material, unsigned int p_atlasIndex) : 
		layerIndex{ 0 },
		atlasIndex{p_atlasIndex},
		material{p_material}
	{};
	Renderable(const Renderable&) = default;

private:
	unsigned int atlasIndex = 0;
	unsigned int layerIndex = 0;
	unsigned int drawCallGroup = 0; //used by layer to determine drawcallgroup?
	Material* material; //Todo: shared_ptr instead of raw pointer. Y tho? could just have a raw pointer tbh.

	friend class Layer;
	friend class Renderer;
};