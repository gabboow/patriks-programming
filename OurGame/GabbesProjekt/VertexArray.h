#pragma once
#include <GL/glew.h>
class VertexArray
{
public:

	VertexArray();
	~VertexArray();

	//move constructor
	VertexArray(VertexArray&& p_other) = delete;
	//copy assignment operator
	VertexArray& operator=(const VertexArray& p_other) = delete;
	//Deleting copy constructor.
	VertexArray(const VertexArray& p_other) = delete;

	void Bind();

private:
	GLuint VA;

};

