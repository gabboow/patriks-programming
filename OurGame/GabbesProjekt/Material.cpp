#include "Material.h"


//Todo: For better efficiency, could allow for same material to have multiple textures.
//that way we could maintain them in a single drawcall.
//How many textures we allow would differ between systems though. Not sure how to work around that.
//I guess renderer could check if we exceed max limit -> start new drawcall.
Material::Material(Texture* p_texture, TextureShader* p_shader) :
	texture{ p_texture },
	shader{ p_shader } {};

Material::~Material() {

};

void Material::Bind() {
	shader->Bind();
	texture->Bind();
};

Texture* Material::GetTexture() {
	return texture;
}

TextureShader* Material::GetShader() {
	return shader;
}

