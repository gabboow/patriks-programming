#include "IndexBuffer.h"



IndexBuffer::IndexBuffer(const unsigned int& p_maxIndexCount) {
	glGenBuffers(1, &IB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * p_maxIndexCount, nullptr, GL_DYNAMIC_DRAW);
}

IndexBuffer::~IndexBuffer() {
	glDeleteBuffers(1, &IB);
}

void IndexBuffer::Bind(const size_t& p_count, const void* p_data ) {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, p_count * sizeof(GLuint), p_data);
}