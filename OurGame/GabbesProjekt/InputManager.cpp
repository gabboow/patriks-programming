#include "InputManager.h"
#include "Engine.h"


#include <iostream>

InputManager::InputManager(Window& p_window) : window{p_window}, scrollState{ScrollState::NOTHING}
 {
	//glfwSetWindowUserPointer(window.GetWindow(), this);
	glfwSetKeyCallback(window.GetWindow(), KeyCallback);
	glfwSetMouseButtonCallback(window.GetWindow(), MouseCallback);
	glfwSetScrollCallback(window.GetWindow(), ScrollCallback);
}

InputManager::~InputManager() {
	glfwSetWindowUserPointer(window.GetWindow(), nullptr);
}

bool InputManager::RegisterKeyInput(int p_scanCode)
{
	glfwGetKey(window.GetWindow(), p_scanCode);
	return false;
}

bool InputManager::FindKey(int p_key)
{
	auto it = keys.find(p_key);

	if (it != keys.end()) {
		return true;
	}
	return false;

}

bool InputManager::IsKeyDownOnce(int p_key)
{

	auto it = keys.find(p_key);
	if (it != keys.end() && it->second == ButtonState::PRESSED) {
		keys.erase(it);
		keys.insert(std::pair(p_key, ButtonState::HOLD));
		return true;
	}

	/*auto it = keys.find(p_key);
	if (it == keys.end()) {
		return false;
	} else if (it->second == HOLD) {
		return false;
	}
	else if (it->second == PRESSED) {
		keys.erase(it);
		keys.insert(std::pair(p_key, HOLD));
		return true;
	}*/

	return false;

}

bool InputManager::IsKeyDown(int p_key)
{
	return FindKey(p_key);
}

void InputManager::KeyCallback(GLFWwindow* p_window, int p_key, int p_scancode, int p_action, int p_mods)
{
	Engine* engine = static_cast<Engine*>(glfwGetWindowUserPointer(p_window));
	InputManager* inputManager = engine->inputManagerPointer;


	std::map<int, ButtonState>& keys = inputManager->keys;
	if (p_action == GLFW_PRESS)
	{
		if (!inputManager->FindKey(p_key))
		{

			keys.insert(std::pair(p_key, ButtonState::PRESSED) );

		}
	} else if (p_action == GLFW_REPEAT)
	{
		if (inputManager->FindKey(p_key))
		{

			keys[p_key] = ButtonState::HOLD;
		}
	} else if (p_action == GLFW_RELEASE)
	{
		keys.erase(p_key);

	}
	//std::cout << "keys: " << keys.size() << '\n';
}

//mouse events here. Should probably move keyboard and mouse into separate classes later on, but whatever.
void InputManager::MouseCallback(GLFWwindow* p_window, int p_button, int p_action, int p_mods) {

	Engine* engine = static_cast<Engine*>( glfwGetWindowUserPointer(p_window) );
	InputManager* inputManager = engine->inputManagerPointer;

	std::map<int, ButtonState>& mouseButtons = inputManager->mouseButtons;
	auto it = mouseButtons.find(p_button);
	if (p_action == GLFW_PRESS) {
		if (it == mouseButtons.end()) {

			mouseButtons.insert(std::pair(p_button, ButtonState::PRESSED));
		}
	} else if (p_action == GLFW_REPEAT) {
		auto it = mouseButtons.find(p_button);
		if (it != mouseButtons.end()) {
			mouseButtons[p_button] = ButtonState::HOLD;
		}
	}
	else if (p_action == GLFW_RELEASE) {
		mouseButtons.erase(p_button);
	}
}


bool InputManager::IsMouseButtonDownOnce(int p_key)
{

	auto it = mouseButtons.find(p_key);
	if (it != mouseButtons.end() && it->second == ButtonState::PRESSED) {
		mouseButtons.erase(it);
		mouseButtons.insert(std::pair(p_key, ButtonState::HOLD));
		return true;
	}

	return false;

}

bool InputManager::IsMouseButtonDown(int p_key)
{
	auto it = mouseButtons.find(p_key);
	if (it != mouseButtons.end()) {
		return true;
	}
	return false;

}

//scrolling
void InputManager::ScrollCallback(GLFWwindow* p_window, double xoffset, double yoffset) {
	//std::cout << "HELLO SCROLL" << '\n';
	//todo: implementation.

	//todo have the engine be pointed to, access inputmanager from engine!

	Engine* engine = static_cast<Engine*>(glfwGetWindowUserPointer(p_window));
	InputManager* inputManager = engine->inputManagerPointer;
	//todo: how do we set it to nothing though? We basically want to wait until frame is done and then remove it?
	//Do I really need to have an update function just for this? ><
	if (yoffset > 0) {
		inputManager->scrollState = ScrollState::UP;
	}
	else if (yoffset < 0) {
		inputManager->scrollState = ScrollState::DOWN;
	}
	else if (yoffset == 0) {
		inputManager->scrollState = ScrollState::NOTHING;
	}

	std::cout << "YOFFSET: " << yoffset << '\n';

}
bool InputManager::IsMouseScrollDown() {
	return scrollState == ScrollState::DOWN ? true : false;
}

bool InputManager::IsMouseScrollUp() {
	return scrollState == ScrollState::UP ? true : false;
}

//todo: this is not needed, could move the polling of glfw to here, and when we call it we reset instead.
void InputManager::Update() { 
	//this is almost not practical, only useful if we haven't called isMouseScrollUp or down from startup.
	//But it is also basically nothing, so may aswell stay.
	scrollState = ScrollState::NOTHING;
}