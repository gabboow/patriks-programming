#pragma once
#include <GL/glew.h>

class IndexBuffer
{
public:

	IndexBuffer(const unsigned int& p_maxIndexCount);
	~IndexBuffer();
	//move constructor
	IndexBuffer(IndexBuffer&& p_other) = delete;
	//copy assignment operator
	IndexBuffer& operator=(const IndexBuffer& p_other) = delete;
	//Deleting copy constructor.
	IndexBuffer(const IndexBuffer& p_other) = delete;


	void Bind(const size_t& p_count, const void* p_data);

private:
	GLuint IB;
};

