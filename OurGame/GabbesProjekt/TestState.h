#pragma once
#include "IState.h"
#include <vector>
#include <GL/glew.h>
#include <array>
#include "BasicShader.h"
#include "TextureShader.h"
#include <SOIL2/SOIL2.h>
#include "Vertex.h"

#include "Texture.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

const unsigned int MAX_QUAD_COUNT = 1;
const unsigned int MAX_VERTEX_COUNT = MAX_QUAD_COUNT * 4;
const unsigned int MAX_INDEX_COUNT = MAX_QUAD_COUNT * 6;

class TestState : public IState {
public:
	TestState();
	~TestState();

	void Enter();
	void Exit();
	void Update(float p_deltaTime);
	void DrawShittyQuad();
	void DrawQuads();

	void Draw();
private:

	std::vector<GLuint> CreateIndices(float p_count);
	std::array<Vertex, 4> CreateQuad(float p_atlasPosition,float p_scale, Vec2 p_position);

	TextureShader textureShader; //order is important here, this is where order of construction happens.

	GLuint& texturedProgramID;


	GLuint& programID;
	//std::array<Vertex, 6> vertexData;
	
	//std::array<Vertex,8> vertexData;
	//std::array <GLuint, 6*2> triangleIndices; //vertex 0,1,2 
	BasicShader testShader;

	Texture texture;

	float time;

	VertexArray va;

	VertexBuffer vb;

	IndexBuffer ib;
	//GLuint vb;

};

