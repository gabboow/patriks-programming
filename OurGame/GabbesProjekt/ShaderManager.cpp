#include "ShaderManager.h"
#include <iostream>

ShaderManager::ShaderManager() {

}
//LET'S NOT USE THIS CLASS, IT IS SOOOO UNECCESSARY.

ShaderManager::~ShaderManager() {
}

void ShaderManager::AddShader(std::string& p_id, std::shared_ptr<IShader>& p_shader) {
	auto it = shaders.find(p_id);

	if (it != shaders.end()) {
		std::cout << "Shader already existed!" << '\n';
		return;
	}
	shaders.insert(std::pair<std::string, std::shared_ptr<IShader>>(p_id, p_shader));
}

IShader* ShaderManager::GetShader(std::string& p_id) {
	auto it = shaders.find(p_id);
	return it == shaders.end() ? nullptr : it->second.get();
}



