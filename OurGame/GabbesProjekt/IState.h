#pragma once


class IState {
public:
	IState();
	virtual ~IState() = 0;

	virtual void Enter() = 0;
	virtual void Exit() = 0;
	virtual void Update(float p_delta) = 0;
	virtual void Draw() = 0;
};
