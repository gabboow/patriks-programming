#include "StateMachine.h"


#include <iostream>


StateMachine::StateMachine() : currentState{nullptr} {

}

StateMachine::~StateMachine() {
}


void StateMachine::AddState(std::string p_key,IState* p_stateToAdd) {
	auto it = states.find(p_key);
	if (it != states.end()) {
		std::cout << "State already exists!" << '\n';
		return;
	}
	else {
		states.insert(std::pair<std::string, IState*>(p_key, p_stateToAdd));
	}
}

void StateMachine::SetState(std::string p_key) {

	auto it = states.find(p_key);
	if (it == states.end()) {
		std::cout << "State does not exist" << '\n';
		return;
	}
	if (currentState != nullptr) {
		currentState->Exit();
	}
	currentState = it->second;
	currentState->Enter();
}


void StateMachine::Update(float p_deltaTime) {
	if (currentState != nullptr) {
		currentState->Update(p_deltaTime);
	}
}

void StateMachine::Draw() {
	if (currentState != nullptr) {
		currentState->Draw();
	}
}