#include "Engine.h"
#include <chrono>
#include "StateMachine.h"
#include "GameplayState.h"
#include "IState.h"
#include "Logger.h"
#include "TestState.h"
#include "TextureFlipper.h"
#include "Camera.h"
#include "Editor.h"
#include "Renderer.h"
#include "Window.h"

#include <functional>

//#include <iostream>



Engine::Engine() : windowPointer{ nullptr }, inputManagerPointer{ nullptr }, cameraPointer{nullptr}
{
}

Engine::~Engine()
{
	windowPointer = nullptr;
	inputManagerPointer = nullptr;
	cameraPointer = nullptr;
}

void Engine::OnResize() {
	uint32_t width = windowPointer->GetWidth();
	uint32_t height = windowPointer->GetHeight();
	//int width, height;
	//glfwGetWindowSize(windowPointer->GetWindow(), &width, &height);


	//some info on how to do this properly here: https://stackoverflow.com/questions/3267243/in-opengl-how-can-i-adjust-for-the-window-being-resized
	float aspectRatio = (float)width / (float)height;
	/**
	if aspectRatio > 1
		xSpan = 1.0f * aspectRatio.
		else
		ySpan = 1.0f / aspectRatio.
	*/

	if (width > height) {
		aspectRatio = (float)width / (float)height;
	}
	else {
		aspectRatio = (float)width / (float)height; //todo: this doesn't work the way i expect it to???
		//aspectRatio = (float)height / (float)width;
	}
	cameraPointer->SetAspectRatio(aspectRatio);

	std::cout << "Resized window callback called!" << '\n';

}

void Engine::Run()
{

	GL_Init gl_init;
	gl_init.InitGLFW();

	const uint32_t windowWidth = 1920/2;
	const uint32_t windowHeight = 1080/2;
	Window window{windowWidth, windowHeight};
	windowPointer = &window;
	glfwSetWindowUserPointer(window.GetWindow(), this);
	glewExperimental = GL_TRUE;
	gl_init.InitGLEW();

	InputManager inputManager{ window };
	inputManagerPointer = &inputManager;

	

	float aspectRatio = (float)windowWidth / (float)windowHeight;
	Camera camera{-1.0f,1.0f, -1.0 ,1.0f, aspectRatio  };
	cameraPointer = &camera;
	window.OnWindowResize(std::bind(&Engine::OnResize, this) );

	camera.SetPosition( { 0.0f,0.0f } );
	//camera.SetRotation({ 45.0f });

	Renderer renderer{ camera.GetViewProjectionMatrix() };
	GameplayState* gameplayState = new GameplayState(renderer);
	StateMachine stateMachine;
	std::string gameplayStateKey = "gameplay";
	stateMachine.AddState(gameplayStateKey, gameplayState);

	TestState* testState = new TestState();
	std::string testStateKey = "test";
	stateMachine.AddState(testStateKey, testState);

	stateMachine.SetState("gameplay"); //todo: I guess in order to properly test, let's try different textures, 100 of different textures.


	Editor editor{ window };


	auto currentTime = std::chrono::high_resolution_clock::now();
	auto previousTime = std::chrono::high_resolution_clock::now();
	bool isRunning = true;


	//float f = 0.0f;
	//ImVec4 color = { 0.0f,0.0f,0.0f,0.0f };

	while( isRunning ) {
		currentTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<float, std::milli> deltaTime = currentTime - previousTime;

		const Vector2& currentCameraPosition = camera.GetPosition();
		if (inputManager.IsKeyDown(GLFW_KEY_S)) {
			camera.SetPosition( { currentCameraPosition.x, currentCameraPosition.y - 1.0f * deltaTime.count() * 0.001f } );
		}
		if (inputManager.IsKeyDown(GLFW_KEY_W)) {
			camera.SetPosition({ currentCameraPosition.x, currentCameraPosition.y + 1.0f * deltaTime.count() * 0.001f });
		}
		if (inputManager.IsKeyDown(GLFW_KEY_A)) {
			camera.SetPosition({ currentCameraPosition.x - 1.0f * deltaTime.count() * 0.001f, currentCameraPosition.y });
		}
		if (inputManager.IsKeyDown(GLFW_KEY_D)) {
			camera.SetPosition({ currentCameraPosition.x + 1.0f * deltaTime.count()*0.001f , currentCameraPosition.y });
		}
		if (inputManager.IsMouseButtonDownOnce(GLFW_MOUSE_BUTTON_LEFT)) {
			//std::cout << "Mouse goes B";
		}
		if (inputManager.IsMouseButtonDown(GLFW_MOUSE_BUTTON_LEFT)) {
			//std::cout << "R";
		}
		if (inputManager.IsMouseScrollUp()) {
			std::cout << "MOVING ON UP!" << '\n';
			camera.Zoom(-10.0f * deltaTime.count()*0.001f);
		} 
		if (inputManager.IsMouseScrollDown()) {
			std::cout << "GOING DOWN!" << '\n';
			camera.Zoom(10.0f * deltaTime.count() * 0.001f);
		}



		glClear(GL_COLOR_BUFFER_BIT);


		stateMachine.Update( deltaTime.count()*0.001f ); //todo: divide update and draw.

		editor.Update();
		//todo: update our game state here.
		inputManager.Update(); //almost entirely useless function.
		previousTime = currentTime;

		stateMachine.Draw();
		editor.Render();
		//end of frame.
		//ImGui::Render();
		//ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapInterval(1);
		glfwSwapBuffers(window.GetWindow());
		glfwPollEvents();
	}


	glfwTerminate();
	delete gameplayState;
	gameplayState = nullptr;
}

//todo: will need to fix correct build for debug / release of...
//glew
//glfw
//imgui - no
//SOIL2 - 
//I guess also json if we opt to use it! - 
