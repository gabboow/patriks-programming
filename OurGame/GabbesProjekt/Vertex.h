#pragma once
#include "Mat4.h"

//Todo: maybe move to components.
struct Vec3 {
	float x, y, z;
	Vec3() = default;
	Vec3(const Vec3&) = default;
};


/*Todo: this is obviously really messy, MOVE MOVE MOVE!**/
inline Vec3 operator*(mat4 p_mat, Vec3 p_vec) //Pretending it's a 4x4 vector.
{
	Vec3 vec; //todo: no idea if 4th component important or not, i guess not? important as it maintains position.
	vec.x = p_mat.m00 * p_vec.x + p_mat.m10 * p_vec.y + p_mat.m20 * p_vec.z + p_mat.m30 * 1.0f;
	vec.y = p_mat.m01 * p_vec.x + p_mat.m11 * p_vec.y + p_mat.m21 * p_vec.z + p_mat.m31 * 1.0f;
	vec.z = p_mat.m02 * p_vec.x + p_mat.m12 * p_vec.y + p_mat.m22 * p_vec.z + p_mat.m32 * 1.0f;
	return vec;
}


struct Vec2 {
	float x, y;
	Vec2() = default;
	Vec2(const Vec2&) = default;
};

//copy & moveable.
struct Vertex {
	Vec3 positions;
	Vec3 colors;
	Vec2 uvs;
	float atlasPosition;
	Vertex() = default;
	Vertex(const Vertex&) = default;

};

