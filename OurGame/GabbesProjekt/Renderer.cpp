#include "Renderer.h"
#include <iostream>
#include "Renderable.h"

//todo: use glSubImage2d or whatever it is called, that way we won't have to bloat the shader -
//todo, mipmap issue: https://gamedev.stackexchange.com/questions/46963/how-to-avoid-texture-bleeding-in-a-texture-atlas

const unsigned int MAX_QUAD_COUNT = 1000;
const unsigned int MAX_VERTEX_COUNT = MAX_QUAD_COUNT * 4;
const unsigned int MAX_INDEX_COUNT = MAX_QUAD_COUNT * 6;

Renderer::Renderer(mat4& p_cameraProjectionViewMatrix) :
					   cameraProjectionViewMatrix{ p_cameraProjectionViewMatrix },
					   vertexArray{},
					   vertexBuffer{MAX_VERTEX_COUNT},
					   indexBuffer{MAX_INDEX_COUNT},
					   registry{nullptr},
					   currentMaterial{nullptr},
					   drawCallCount{0}
{
	buffer = new Vertex[MAX_VERTEX_COUNT];
	bufferPointer = nullptr;
	bufferSize = 0;
	quadCount = 0;
}

Renderer::~Renderer() {
	delete[] buffer;
	bufferPointer = nullptr;
}

void Renderer::SetRegistry(entt::registry* p_registry) {
	registry = p_registry;
}

void Renderer::BeginDrawCall(Material* p_material) { //TODO: get camera here.
	drawCallCount++;
	currentMaterial = p_material;
	p_material->Bind();
	bufferPointer = buffer; //point to beginning.
}

//between begin and end is where you'd update uniforms you want updated every frame.
void Renderer::EndDrawCall(TextureShader* p_shader) {
	const unsigned int indexCount = quadCount * 6;
	std::vector<GLuint> indices = CreateIndices(quadCount);

	vertexArray.Bind();
	vertexBuffer.Bind(bufferSize, buffer);
	indexBuffer.Bind(/*indexCount*/indices.size(), indices.data() ); //todo: indices.


	p_shader->UpdateVertices();
	p_shader->SetUniformMat4("viewProjection", cameraProjectionViewMatrix);

	glDrawElements(GL_TRIANGLES, 6 * quadCount, GL_UNSIGNED_INT, 0);

	quadCount = 0;
	bufferSize = 0;
}

void Renderer::ResetDrawCalls() {
	//std::cout << "Draw calls: " << drawCallCount << '\n';
	drawCallCount = 0;
}


//TODO: move indices to heap!
std::vector<GLuint> Renderer::CreateIndices(unsigned int p_count) {

	std::vector<GLuint> indices;
	for (unsigned int i = 0; i < p_count; i++) {
		const GLuint step = 4 * i;
		indices.push_back(0 + step);
		indices.push_back(1 + step);
		indices.push_back(2 + step);
		indices.push_back(1 + step);
		indices.push_back(3 + step);
		indices.push_back(2 + step);
	}
	return indices;
}


//Needs to be called before rendering layers or we may not draw everything.
//layers should probably also be stored in the renderer.
void Renderer::SortLayers() { //todo: only do if needed instead of each loop.
	assert(registry != nullptr);
	registry->sort<Renderable>([](const auto& lhs, const auto& rhs) {
		return lhs.layerIndex < rhs.layerIndex;
	});
}

void Renderer::RenderQuad(Transform& p_transform,float p_atlasPosition/*, float p_scale, Vector2 p_position*/) {

	//todo: if we are out of memory, end drawcall and begin a new one. (store the latest material used.)
	if (quadCount >= MAX_QUAD_COUNT) {
		EndDrawCall( currentMaterial->GetShader() );
		BeginDrawCall(currentMaterial);
	}

	assert(bufferPointer != nullptr); //Todo: not have scale and position here  Iguess???

	if (bufferPointer != nullptr) { //should be fine to remove but i get a warning.
		
		//TODO: rename "positions into "model", then add uniform position!, or just modelPosition? I mean it is implied that it is a vertex position."
		bufferPointer->positions = p_transform.matrix * Vec3{ -0.5f, -0.5f, 1.0f }; //bottom left
		bufferPointer->colors = { 0.0f, 0.0f, 1.0f };
		bufferPointer->uvs = { 0.0f, 1.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = p_transform.matrix * Vec3{ -0.5f,0.5f,1.0f }; //top left
		bufferPointer->colors = { 0.0f, 1.0f, 0.0f };
		bufferPointer->uvs = { 0.0f, 0.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = p_transform.matrix* Vec3{ 0.5f, -0.5f, 1.0f }; //bottom right
		bufferPointer->colors = { 0.0f, 1.0f, 0.0f };
		bufferPointer->uvs = { 1.0f,1.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = p_transform.matrix* Vec3{ 0.5f,0.5f,1.0f }; //top right
		bufferPointer->colors = { 1.0f, 0.0f, 0.0f };
		bufferPointer->uvs = { 1.0f,0.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		//vec.x = p_mat.m00 * p_vec.x + p_mat.m10 * p_vec.y + p_mat.m20 * p_vec.z;
		//vec.y = p_mat.m01 * p_vec.x + p_mat.m11 * p_vec.y + p_mat.m21 * p_vec.z;


		/*bufferPointer->positions = { -1.f * p_scale + p_position.x, -1.f * p_scale + p_position.y, 0.0f }; //bottom left
		bufferPointer->colors = { 0.0f, 0.0f, 1.0f };
		bufferPointer->uvs = { 0.0f, 1.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = { -1.0f * p_scale + p_position.x ,1.0f * p_scale + p_position.y,0.0f }; //top left
		bufferPointer->colors = { 0.0f, 1.0f, 0.0f };
		bufferPointer->uvs = { 0.0f, 0.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = { 1.0f * p_scale + p_position.x, -1.f * p_scale + p_position.y, 0.0f }; //bottom right
		bufferPointer->colors = { 0.0f, 1.0f, 0.0f };
		bufferPointer->uvs = { 1.0f,1.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;

		bufferPointer->positions = { 1.0f * p_scale + p_position.x,1.0f * p_scale + p_position.y,0.0f }; //top right
		bufferPointer->colors = { 1.0f, 0.0f, 0.0f };
		bufferPointer->uvs = { 1.0f,0.0f };
		bufferPointer->atlasPosition = p_atlasPosition;
		bufferPointer++;*/

		bufferSize += 4;
		quadCount++;
	}

}