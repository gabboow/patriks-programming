#include "Texture.h"
#include <SOIL2/SOIL2.h>



Texture::Texture(const std::string& p_filePath) {
	//creating texture.
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	//Wrapping parameters.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//filtering parameters.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	int WimageWidth, WimageHeight, Wchannels;
	unsigned char* Wimage = SOIL_load_image(p_filePath.c_str(), &WimageWidth, &WimageHeight, &Wchannels, SOIL_LOAD_RGBA);
	//FlipTexture(Wimage, WimageWidth, WimageHeight, Wchannels);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, WimageWidth, WimageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, Wimage);
	//glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(Wimage);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture() {
	glDeleteTextures(1, &textureID);
}

void Texture::Bind() {
	glBindTexture(GL_TEXTURE_2D, textureID);
}

