#include "VertexArray.h"

//create and immediately bind.
VertexArray::VertexArray()
{
	glCreateVertexArrays(1, &VA);
	glBindVertexArray(VA);
}

VertexArray::~VertexArray() {
	glDeleteVertexArrays(1, &VA);
}

void VertexArray::Bind() {
	glBindVertexArray(VA);
}


