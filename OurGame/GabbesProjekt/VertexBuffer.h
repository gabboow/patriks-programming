#pragma once
#include <GL/glew.h>

class VertexBuffer
{
public:


	VertexBuffer(const unsigned int& P_MAX_VERTEX_COUNT);
	~VertexBuffer();
	//move constructor
	VertexBuffer(VertexBuffer&& p_other) = delete;
	//copy assignment operator
	VertexBuffer& operator=(const VertexBuffer& p_other) = delete;
	//Deleting copy constructor.
	VertexBuffer(const VertexBuffer& p_other) = delete;

	void Bind(const size_t& p_count,const void* p_data);

private:
	GLuint VB;

};

