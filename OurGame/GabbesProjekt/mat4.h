#pragma once

//from an old course in uni.

//todo: put into namespace or something.

#include <math.h>
#define PI  3.1415926535897932384626433832795

union mat4 {
    struct {
        float m00, m01, m02, m03;
        float m10, m11, m12, m13;
        float m20, m21, m22, m23;
        float m30, m31, m32, m33;
    };
    float m[16];
};

inline mat4 operator* (mat4 L, mat4 R) {
    mat4 m;

    m.m[0] = L.m[0] * R.m[0] + L.m[4] * R.m[1] + L.m[8] * R.m[2] + L.m[12] * R.m[3];
    m.m[4] = L.m[0] * R.m[4] + L.m[4] * R.m[5] + L.m[8] * R.m[6] + L.m[12] * R.m[7];
    m.m[8] = L.m[0] * R.m[8] + L.m[4] * R.m[9] + L.m[8] * R.m[10] + L.m[12] * R.m[11];
    m.m[12] = L.m[0] * R.m[12] + L.m[4] * R.m[13] + L.m[8] * R.m[14] + L.m[12] * R.m[15];

    m.m[1] = L.m[1] * R.m[0] + L.m[5] * R.m[1] + L.m[9] * R.m[2] + L.m[13] * R.m[3];
    m.m[5] = L.m[1] * R.m[4] + L.m[5] * R.m[5] + L.m[9] * R.m[6] + L.m[13] * R.m[7];
    m.m[9] = L.m[1] * R.m[8] + L.m[5] * R.m[9] + L.m[9] * R.m[10] + L.m[13] * R.m[11];
    m.m[13] = L.m[1] * R.m[12] + L.m[5] * R.m[13] + L.m[9] * R.m[14] + L.m[13] * R.m[15];

    m.m[2] = L.m[2] * R.m[0] + L.m[6] * R.m[1] + L.m[10] * R.m[2] + L.m[14] * R.m[3];
    m.m[6] = L.m[2] * R.m[4] + L.m[6] * R.m[5] + L.m[10] * R.m[6] + L.m[14] * R.m[7];
    m.m[10] = L.m[2] * R.m[8] + L.m[6] * R.m[9] + L.m[10] * R.m[10] + L.m[14] * R.m[11];
    m.m[14] = L.m[2] * R.m[12] + L.m[6] * R.m[13] + L.m[10] * R.m[14] + L.m[14] * R.m[15];

    m.m[3] = L.m[3] * R.m[0] + L.m[7] * R.m[1] + L.m[11] * R.m[2] + L.m[15] * R.m[3];
    m.m[7] = L.m[3] * R.m[4] + L.m[7] * R.m[5] + L.m[11] * R.m[6] + L.m[15] * R.m[7];
    m.m[11] = L.m[3] * R.m[8] + L.m[7] * R.m[9] + L.m[11] * R.m[10] + L.m[15] * R.m[11];
    m.m[15] = L.m[3] * R.m[12] + L.m[7] * R.m[13] + L.m[11] * R.m[14] + L.m[15] * R.m[15];

    return m;
}

inline mat4 identity() {
    return {
      1.0f,0.0f,0.0f,0.0f,
      0.0f,1.0f,0.0f,0.0f,
      0.0f,0.0f,1.0f,0.0f,
      0.0f,0.0f,0.0f,1.0f
    };
}

inline mat4 translation(float x, float y, float z) {
    return {
      1.0f,0.0f,0.0f,0.0f,
      0.0f,1.0f,0.0f,0.0f,
      0.0f,0.0f,1.0f,0.0f,
      x,y,z,1.0f
    };
}

inline mat4 scale(float s) {
    mat4 m = {
        s,0.f,0.f,0.f,
        0.f,s,0.f,0.f,
        0.f,0.f,s,0.f,
        0.f,0.f,0.f,1.f };
    return m;
}

inline mat4 scale(float x, float y, float z) {
    mat4 m = { x,0.f,0.f,0.f,  0.f,y,0.f,0.f,  0.f,0.f,z,0.f,  0.f,0.f,0.f,1.f };
    return m;
}

inline mat4 rotationZ(float radians) {
    float cos = cosf(radians);
    float sin = sinf(radians);
    mat4 m = {
      cos, sin, 0.0f, 0.0f,
      -sin, cos, 0.0f, 0.0f,
      0.0f, 0.0f, 1.0f, 0.0f,
      0.0f, 0.0f, 0.0f, 1.0f
    };
    return m;
}

//https://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix user willnode
inline mat4 invertMatrix(const mat4 m) { //todo: this assumes row major? column major? Fack if i know mate. but prolly wrong yeh.
    float A2323 = m.m22 * m.m33 - m.m23 * m.m32;
    float A1323 = m.m21 * m.m33 - m.m23 * m.m31;
    float A1223 = m.m21 * m.m32 - m.m22 * m.m31;
    float A0323 = m.m20 * m.m33 - m.m23 * m.m30;
    float A0223 = m.m20 * m.m32 - m.m22 * m.m30;
    float A0123 = m.m20 * m.m31 - m.m21 * m.m30;
    float A2313 = m.m12 * m.m33 - m.m13 * m.m32;
    float A1313 = m.m11 * m.m33 - m.m13 * m.m31;
    float A1213 = m.m11 * m.m32 - m.m12 * m.m31;
    float A2312 = m.m12 * m.m23 - m.m13 * m.m22;
    float A1312 = m.m11 * m.m23 - m.m13 * m.m21;
    float A1212 = m.m11 * m.m22 - m.m12 * m.m21;
    float A0313 = m.m10 * m.m33 - m.m13 * m.m30;
    float A0213 = m.m10 * m.m32 - m.m12 * m.m30;
    float A0312 = m.m10 * m.m23 - m.m13 * m.m20;
    float A0212 = m.m10 * m.m22 - m.m12 * m.m20;
    float A0113 = m.m10 * m.m31 - m.m11 * m.m30;
    float A0112 = m.m10 * m.m21 - m.m11 * m.m20;

    float det = m.m00 * (m.m11 * A2323 - m.m12 * A1323 + m.m13 * A1223)
        - m.m01 * (m.m10 * A2323 - m.m12 * A0323 + m.m13 * A0223)
        + m.m02 * (m.m10 * A1323 - m.m11 * A0323 + m.m13 * A0123)
        - m.m03 * (m.m10 * A1223 - m.m11 * A0223 + m.m12 * A0123);
    det = 1 / det;

    return {
       det * (m.m11 * A2323 - m.m12 * A1323 + m.m13 * A1223),
       det * -(m.m01 * A2323 - m.m02 * A1323 + m.m03 * A1223),
       det * (m.m01 * A2313 - m.m02 * A1313 + m.m03 * A1213),
       det * -(m.m01 * A2312 - m.m02 * A1312 + m.m03 * A1212),
       det * -(m.m10 * A2323 - m.m12 * A0323 + m.m13 * A0223),
       det * (m.m00 * A2323 - m.m02 * A0323 + m.m03 * A0223),
       det * -(m.m00 * A2313 - m.m02 * A0313 + m.m03 * A0213),
       det * (m.m00 * A2312 - m.m02 * A0312 + m.m03 * A0212),
       det * (m.m10 * A1323 - m.m11 * A0323 + m.m13 * A0123),
       det * -(m.m00 * A1323 - m.m01 * A0323 + m.m03 * A0123),
       det * (m.m00 * A1313 - m.m01 * A0313 + m.m03 * A0113),
       det * -(m.m00 * A1312 - m.m01 * A0312 + m.m03 * A0112),
       det * -(m.m10 * A1223 - m.m11 * A0223 + m.m12 * A0123),
       det * (m.m00 * A1223 - m.m01 * A0223 + m.m02 * A0123),
       det * -(m.m00 * A1213 - m.m01 * A0213 + m.m02 * A0113),
       det * (m.m00 * A1212 - m.m01 * A0212 + m.m02 * A0112),
    };
}


inline mat4 ortho(float left, float right, float bottom, float top, float nearZ, float farZ) {
    float a = 2.0f / (right - left); //MULTIPLYING BY 0.5F IS NOOOOOT THE SAME WHEN WE DIVIDE. LOL.
    float b = 2.0f / (top - bottom);
    float c = -2.0f / (farZ - nearZ);
    float x = -(right + left) / (right - left);
    float y = -(top + bottom) / (top - bottom);
    float z = -(farZ + nearZ) / (farZ - nearZ);
    mat4 m = {
      a, 0.0f, 0.0f, 0.0f,
      0.0f, b, 0.0f, 0.0f,
      0.0f, 0.0f, c, 0.0f,
      x, y, z, 1.0f
    };
    return m;
}
//Result[0][0] = static_cast<T>(2) / (right - left);
//Result[1][1] = static_cast<T>(2) / (top - bottom);
//Result[3][0] = -(right + left) / (right - left);
//Result[3][1] = -(top + bottom) / (top - bottom);

